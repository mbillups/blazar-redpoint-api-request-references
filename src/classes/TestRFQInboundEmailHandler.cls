@isTest
private class TestRFQInboundEmailHandler
{
	static Messaging.InboundEmail email;
	static Messaging.InboundEmail modEmail;
	static Messaging.InboundEmail modEmail2;

	static void setup()
	{
		List<Messaging.InboundEmail.TextAttachment> textAtts = new List<Messaging.InboundEmail.TextAttachment>();
		Messaging.InboundEmail.TextAttachment ta = new Messaging.InboundEmail.TextAttachment();
		ta.body = 'Text Attachment';
		ta.FileName = 'Text Attachment FileName.csv';
		textAtts.add(ta);

		Messaging.InboundEmail.TextAttachment ta2 = new Messaging.InboundEmail.TextAttachment();
		ta2.body = 'Text Attachment 2';
		ta2.FileName = 'Text Attachment FileName 2.csx'; // for testing Aspose.
		textAtts.add(ta2);

		Messaging.InboundEmail.TextAttachment ta3 = new Messaging.InboundEmail.TextAttachment();
		ta3.body = 'Text Attachment 3';
		ta3.FileName = 'Text Attachment FileName 2.ocr'; // for testing Aspose.
		textAtts.add(ta3);

		Messaging.InboundEmail.TextAttachment ta4 = new Messaging.InboundEmail.TextAttachment();
		ta4.body = 'Text Attachment 4';
		ta4.FileName = 'Text Attachment FileName 2.docx'; // for testing Aspose.
		textAtts.add(ta4);

		Messaging.InboundEmail.TextAttachment ta5 = new Messaging.InboundEmail.TextAttachment();
		ta5.body = 'Text Attachment 5';
		ta5.FileName = 'Text Attachment FileName 2.xlsx'; // for testing Aspose.
		textAtts.add(ta5);

		email = new Messaging.InboundEmail();
		email.binaryAttachments = null;
		email.textAttachments = textAtts;
		email.fromAddress = 'joe@foo.com';
		email.subject = 'Test Email';
		email.plainTextBody = 'This email is being sent to you from the SEWP V QRT Tool.\n\n';
		email.plainTextBody = email.plainTextBody + 'Email Type : NEW Request\n\n';
		email.plainTextBody = email.plainTextBody + 'Request Type : Request For Quote\n';
		email.plainTextBody = email.plainTextBody + 'SEWP Version : V\n';
		email.plainTextBody = email.plainTextBody + 'Request ID# : 49422\n';
		email.plainTextBody = email.plainTextBody + 'Agency ID : AFO-336_ARC\n';
		email.plainTextBody = email.plainTextBody + 'Subject : Open Splice and Vortex Dev. Lic. Support (NEW, not renewal)\n';
		email.plainTextBody = email.plainTextBody + 'Request Date : 21-DEC-2016\n';
		email.plainTextBody = email.plainTextBody + 'Reply by Date : 29-DEC-2016 23:59\n';
		email.plainTextBody = email.plainTextBody + 'Mod Level : 0\n';
		email.plainTextBody = email.plainTextBody + 'Mod Date : 21-DEC-2016\n';
		email.plainTextBody = email.plainTextBody + 'First Name : SERVI\n';
		email.plainTextBody = email.plainTextBody + 'Last Name : NASA-AMES\n';
		email.plainTextBody = email.plainTextBody + 'Phone Number : (650) 604-XXXX\n';
		email.plainTextBody = email.plainTextBody + 'Email : arc-dl-servi@mail.nasa.gov<mailto:arc-dl-servi@mail.nasa.gov>\n';
		email.plainTextBody = email.plainTextBody + 'Agency : National Aeronautics and Space Administration\n';
		email.plainTextBody = email.plainTextBody + 'Address1 : none supplied\n';
		email.plainTextBody = email.plainTextBody + 'Address2 : none supplied\n';
		email.plainTextBody = email.plainTextBody + 'Address3 : \n';
		email.plainTextBody = email.plainTextBody + 'Address4 : \n';
		email.plainTextBody = email.plainTextBody + 'City : none supplied\n';
		email.plainTextBody = email.plainTextBody + 'State : CA\n';
		email.plainTextBody = email.plainTextBody + 'Zip : 55555\n';
		email.plainTextBody = email.plainTextBody + 'CONUS : Contiguous United States\n';
		email.plainTextBody = email.plainTextBody + 'Agency : National Aeronautics and Space Administration\n';
		email.plainTextBody = email.plainTextBody + 'Attachments included : Y\n';
		email.plainTextBody = email.plainTextBody + 'Attachments online : Y\n';
		email.plainTextBody = email.plainTextBody + 'Attachments : rfq_49422_526856_AFO-366_Snip4RFQ.docx\n';
		email.plainTextBody = email.plainTextBody + 'GPAT : 0\n';
		email.plainTextBody = email.plainTextBody + 'Requirements : see attachment\n';
		email.plainTextBody = email.plainTextBody + 'MOD Remarks : \n\n\n';
		email.plainTextBody = email.plainTextBody + '**********End of machine readable section**********\n';

		modEmail = new Messaging.InboundEmail();
		modEmail.binaryAttachments = null;
		modEmail.fromAddress = 'joe@foo.com';
		modEmail.subject = 'MOD Test Email';
		modEmail.plainTextBody = 'MOD This email is being sent to you from the SEWP V QRT Tool.\n\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'Email Type : MOD/Update to Request\n\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'Request Type : Request For Quote\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'SEWP Version : V\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'Request ID# : 49422\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'Agency ID : AFO-336_ARC\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'Subject : MOD Open Splice and Vortex Dev. Lic. Support (NEW, not renewal)\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'Request Date : 21-DEC-2016\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'Reply by Date : 29-DEC-2016 23:59\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'Mod Level : 1\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'Mod Date : 22-DEC-2016\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'First Name : SERVI\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'Last Name : NASA-AMES\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'Phone Number : (650) 604-XXXX\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'Email : arc-dl-servi@mail.nasa.gov\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'Agency : National Aeronautics and Space Administration\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'Address1 : none supplied\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'Address2 : none supplied\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'Address3 : \n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'Address4 : \n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'City : none supplied\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'State : CA\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'Zip : none supplied\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'CONUS : Contiguous United States\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'Agency : National Aeronautics and Space Administration\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'Attachments included : Y\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'Attachments online : Y\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'Attachments : rfq_49422_526856_AFO-366_Snip4RFQ.docx\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'GPAT : 0\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'Requirements : Evaluation Criteria: Award will be made to the lowest price offeror. The successful offeror will provide delivery of the products listed in the Request for Quote no later than 10 days after receipt of a delivery order.\n';
		modEmail.plainTextBody = modEmail.plainTextBody + 'MOD Remarks : The purpose of this modification is to: 1. Extend the response period of this RFQ to 12/23/2016, Noon, EST.\n2. Indicate that the period of performance for this software maintenance shall be from 1/1/17 - 12/31/17.\n';
		modEmail.plainTextBody = modEmail.plainTextBody + '**********End of machine readable section**********\n';

		modEmail2 = new Messaging.InboundEmail();
		modEmail2.binaryAttachments = null;
		modEmail2.fromAddress = 'joe@foo.com';
		modEmail2.subject = 'MOD Test Email';
		modEmail2.plainTextBody = 'MOD This email is being sent to you from the SEWP V QRT Tool.\n\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'Email Type : MOD/Update to Request\n\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'Request Type : Request For Quote\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'SEWP Version : V\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'Request ID# : 49422\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'Agency ID : AFO-336_ARC\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'Subject : MOD Open Splice and Vortex Dev. Lic. Support (NEW, not renewal)\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'Request Date : 21-DEC-2016\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'Reply by Date : 29-DEC-2016 23:59\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'Mod Level : 1\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'Mod Date : 23-DEC-2016\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'First Name : SERVI\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'Last Name : NASA-AMES\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'Phone Number : (650) 604-XXXX\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'Email : arc-dl-servi@mail.nasa.gov\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'Agency : National Aeronautics and Space Administration\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'Address1 : none supplied\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'Address2 : none supplied\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'Address3 : \n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'Address4 : \n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'City : none supplied\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'State : CA\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'Zip : none supplied\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'CONUS : Contiguous United States\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'Agency : National Aeronautics and Space Administration\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'Attachments included : Y\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'Attachments online : Y\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'Attachments : rfq_49422_526856_AFO-366_Snip4RFQ.docx\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'GPAT : 0\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'Requirements : Evaluation Criteria: Award will be made to the lowest price offeror. The successful offeror will provide delivery of the products listed in the Request for Quote no later than 10 days after receipt of a delivery order.\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + 'MOD Remarks : The purpose of this modification is to: 1. Extend the response period of this RFQ to 12/23/2016, Noon, EST.\n2. Indicate that the period of performance for this software maintenance shall be from 1/1/17 - 12/31/17.\n';
		modEmail2.plainTextBody = modEmail2.plainTextBody + '**********End of machine readable section**********\n';

		Account acct = new Account();
		acct.Name = 'Test Account';
		insert acct;

		Contact ct = new Contact();
		ct.LastName = 'Schmo';
		ct.Email ='foo@schmo.com';
		ct.AccountId = acct.Id;
		insert ct;

		SearchTerm__c prim = new SearchTerm__c();
		prim.IsActive__c = true;
		prim.Name = 'splice';
		prim.TermPadding__c = 'Leading and Trailing spaces';
		insert prim;

		SearchTerm__c sec = new SearchTerm__c();
		sec.IsActive__c = true;
		sec.Name = 'vortex';
		sec.TermPadding__c = 'Leading and Trailing spaces';
		insert sec;

		SearchTerm__c ter = new SearchTerm__c();
		ter.IsActive__c = true;
		ter.Name = 'support';
		ter.TermPadding__c = 'Leading and Trailing spaces';
		insert ter;

		RoutingRule__c rule = new RoutingRule__c();
		rule.PrimaryTerm__c = prim.Id;
		rule.SecondaryTerm__c = sec.Id;
		rule.TertiaryTerm__c = ter.Id;
		rule.IsActive__c = true;
		rule.EmailServiceType__c = 'All';
		insert rule;

		OutboundRecipient__c recip = new OutboundRecipient__c();
		recip.Contact__c = ct.Id;
		recip.RoutingRule__c = rule.Id;
		insert recip;
	}

	@isTest
	static void testEmail()
	{
		setup();
		
		Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
		RFQInboundEmailHandler handler = new RFQInboundEmailHandler();
		handler.handleInboundEmail(email, env);
	}

	@isTest
	static void testMod()
	{
		setup();

		System.debug('**** Starting test Mod');
		
		Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
		RFQInboundEmailHandler handler = new RFQInboundEmailHandler();
		handler.handleInboundEmail(email, env);

		handler.handleInboundEmail(modEmail, env);

		System.debug('**** Done test Mod');
	}

	@isTest
	static void testSEWPV()
	{
		setup();

		EmailSettings__c setting = new EmailSettings__c();
		setting.Use_Aspose__c = true;
		setting.Aspose_Key__c ='key';
		setting.Aspose_ID__c = 'id';
		insert setting;
		
		Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
		SEWPInboundEmailHandler handler = new SEWPInboundEmailHandler();
		handler.handleInboundEmail(email, env);
	}

	@isTest
	static void testSEWP_SDVO()
	{
		setup();

		EmailSettings__c setting = new EmailSettings__c();
		setting.Use_Aspose__c = true;
		setting.Aspose_Key__c = null; // will cause exception in AsposeService. We want that
		setting.Aspose_ID__c = null;
		insert setting;
		
		Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
		SEWP_SDVOInboundEmailHandler handler = new SEWP_SDVOInboundEmailHandler();
		handler.handleInboundEmail(email, env);
	}

	@isTest
	static void testFirstSource()
	{
		setup();
		
		Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
		FirstSourceInboundEmailHandler handler = new FirstSourceInboundEmailHandler();
		handler.handleInboundEmail(email, env);
	}

	@isTest
	static void testOpen()
	{
		setup();
		
		Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
		OpenMarketInboundEmailHandler handler = new OpenMarketInboundEmailHandler();
		handler.handleInboundEmail(email, env);
	}

	@isTest
	static void testNetcents()
	{
		setup();
		
		Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
		NETCENTSInboundEmailHandler handler = new NETCENTSInboundEmailHandler();
		handler.handleInboundEmail(email, env);
	}

	// CIO is tested in its own class

	@isTest
	static void testReprocess()
	{
		setup();

		InboundEmail__c oldEmail = new InboundEmail__c();
		oldEmail.From_Address__c = 'joe@foo.com';
		oldEmail.Subject__c = 'Test Email';
		oldEmail.Text_Body__c = 'This email is being sent to you from the SEWP V QRT Tool.\n\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Email Type : NEW Request\n\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Request Type : Request For Quote\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'SEWP Version : V\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Request ID# : 49422\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Agency ID : AFO-336_ARC\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Subject : Open Splice and Vortex Dev. Lic. Support (NEW, not renewal)\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Request Date : 21-DEC-2016\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Reply by Date : 29-DEC-2016 23:59\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Mod Level : 0\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Mod Date : 21-DEC-2016\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'First Name : SERVI\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Last Name : NASA-AMES\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Phone Number : (650) 604-XXXX\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Email : arc-dl-servi@mail.nasa.gov\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Agency : National Aeronautics and Space Administration\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Address1 : none supplied\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Address2 : none supplied\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Address3 : \n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Address4 : \n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'City : none supplied\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'State : CA\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Zip : none supplied\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'CONUS : Contiguous United States\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Agency : National Aeronautics and Space Administration\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Attachments included : Y\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Attachments online : Y\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Attachments : rfq_49422_526856_AFO-366_Snip4RFQ.docx\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'GPAT : 0\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Requirements : see attachment\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'MOD Remarks : \n\n\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + '**********End of machine readable section**********\n';

		oldEmail.RFQ__c = null;
		oldEmail.Has_Attachment__c = false;
		oldEmail.Outbound_Email_Sent__c = false;
		oldEmail.RFQ_Modification__c = null;
		insert oldEmail;
		
		Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
		SEWPInboundEmailHandler handler = new SEWPInboundEmailHandler();
		handler.isReprocessing = true;
		handler.handleInboundEmail(email, env);
	}

	@isTest
	static void testReprocessError()
	{
		setup();
		
		Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
		SEWPInboundEmailHandler handler = new SEWPInboundEmailHandler();
		handler.isReprocessing = true;
		Messaging.InboundEmailResult result = handler.handleInboundEmail(email, env);
		System.assert(result.success == false);
	}

	@isTest
	static void testDuplicate()
	{
		setup();

		Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
		RFQInboundEmailHandler handler = new RFQInboundEmailHandler();
		handler.handleInboundEmail(email, env);
		handler.handleInboundEmail(email, env);

		Integer ct = [select count() from RFQ_RFI__c];
		System.assert(ct == 1);
	}

	@isTest
	static void testDuplicateMod()
	{
		setup();

		Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
		RFQInboundEmailHandler handler = new RFQInboundEmailHandler();
		handler.handleInboundEmail(email, env);

		handler.handleInboundEmail(modEmail, env);
		handler.handleInboundEmail(modEmail, env);

		Integer ct = [select count() from RFQ_Modification__c];
		System.assert(ct == 1);
	}

	@isTest
	static void test2ModsWithDiffDates()
	{
		setup();

		Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
		RFQInboundEmailHandler handler = new RFQInboundEmailHandler();
		handler.handleInboundEmail(email, env);

		handler.handleInboundEmail(modEmail, env);
		handler.handleInboundEmail(modEmail2, env);

		Integer ct = [select count() from RFQ_Modification__c];
		System.assert(ct == 2);
	}
}