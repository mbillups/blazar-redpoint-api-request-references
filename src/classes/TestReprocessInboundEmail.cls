@isTest
private class TestReprocessInboundEmail
{
	static InboundEmail__c cioEmail;
	static InboundEmail__c cioEmailError;
	static InboundEmail__c sewpEmail;

	static void setup()
	{
		cioEmail = new InboundEmail__c();
		cioEmail.From_Address__c = 'joe@foo.com';
		cioEmail.Subject__c = 'RFQ CS-38204-SB Questions and Answers Released';
		cioEmail.RFQ__c = null;
		cioEmail.Has_Attachment__c = false;
		cioEmail.Outbound_Email_Sent__c = false;
		cioEmail.RFQ_Modification__c = null;
		cioEmail.Text_Body__c = 'Questions and answers for the order RFQ CS-38204-SB has been released\n\n';
		cioEmail.Text_Body__c = cioEmail.Text_Body__c + 'Please visit e-GOS at https://cio.egos.nih.gov for detailed information.\n\n'; 
		cioEmail.Text_Body__c = cioEmail.Text_Body__c + 'E-GOS ID: CS-38204-SB\n';
		cioEmail.Text_Body__c = cioEmail.Text_Body__c + 'Request: RFQ\n';
		cioEmail.Text_Body__c = cioEmail.Text_Body__c + 'Title: EXXACT Spectrum TXR410-0032R Graphics Processing Unit (GPU)\n';
		cioEmail.Text_Body__c = cioEmail.Text_Body__c + 'Description: 1 EACH - Spectrum TXR410-0032R with the following minimum specifications:256GB DDR44 x GTX 1080 PascalNIVIDIA DIGITSHardware\n';
		cioEmail.Text_Body__c = cioEmail.Text_Body__c + 'Government Customer: Department of Health and Human Services, NIH, CC\n\n';
		cioEmail.Text_Body__c = cioEmail.Text_Body__c + 'You can view them by logging into e-GOS and clicking on the Documents tab for RFQ CS-38204-SB .\n\n';
		cioEmail.Email_Service_Type__c = 'CIO-CS';
		insert cioEmail;

		cioEmailError = new InboundEmail__c();
		cioEmailError.From_Address__c = 'joe@foo.com';
		cioEmailError.Subject__c = 'ERROR RFQ CS-38204-SB Questions and Answers Released';
		cioEmailError.RFQ__c = null;
		cioEmailError.Has_Attachment__c = false;
		cioEmailError.Outbound_Email_Sent__c = false;
		cioEmailError.RFQ_Modification__c = null;
		cioEmailError.Text_Body__c = 'Questions and answers for the order RFQ CS-38204-SB has been released\n\n';
		cioEmailError.Text_Body__c = cioEmailError.Text_Body__c + 'Please visit e-GOS at https://cio.egos.nih.gov for detailed information.\n\n'; 
		cioEmailError.Text_Body__c = cioEmailError.Text_Body__c + 'E-GOS ID: CS-38204-SB\n';
		cioEmailError.Text_Body__c = cioEmailError.Text_Body__c + 'Request: RFQ\n';
		cioEmailError.Text_Body__c = cioEmailError.Text_Body__c + 'Title: EXXACT Spectrum TXR410-0032R Graphics Processing Unit (GPU)\n';
		cioEmailError.Text_Body__c = cioEmailError.Text_Body__c + 'Description: 1 EACH - Spectrum TXR410-0032R with the following minimum specifications:256GB DDR44 x GTX 1080 PascalNIVIDIA DIGITSHardware\n';
		cioEmailError.Text_Body__c = cioEmailError.Text_Body__c + 'Government Customer: Department of Health and Human Services, NIH, CC\n\n';
		cioEmailError.Text_Body__c = cioEmailError.Text_Body__c + 'You can view them by logging into e-GOS and clicking on the Documents tab for RFQ CS-38204-SB .\n\n';
		cioEmailError.Email_Service_Type__c = null;
		insert cioEmailError;

		sewpEmail = new InboundEmail__c();
		sewpEmail.From_Address__c = 'joe@foo.com';
		sewpEmail.Subject__c = 'Test Email';
		sewpEmail.Text_Body__c = 'This email is being sent to you from the SEWP V QRT Tool.\n\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'Email Type : NEW Request\n\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'Request Type : Request For Quote\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'SEWP Version : V\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'Request ID# : 49422\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'Agency ID : AFO-336_ARC\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'Subject : Open Splice and Vortex Dev. Lic. Support (NEW, not renewal)\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'Request Date : 21-DEC-2016\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'Reply by Date : 29-DEC-2016 23:59\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'Mod Level : 0\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'Mod Date : 21-DEC-2016\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'First Name : SERVI\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'Last Name : NASA-AMES\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'Phone Number : (650) 604-XXXX\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'Email : arc-dl-servi@mail.nasa.gov\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'Agency : National Aeronautics and Space Administration\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'Address1 : none supplied\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'Address2 : none supplied\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'Address3 : \n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'Address4 : \n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'City : none supplied\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'State : CA\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'Zip : none supplied\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'CONUS : Contiguous United States\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'Agency : National Aeronautics and Space Administration\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'Attachments included : Y\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'Attachments online : Y\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'Attachments : rfq_49422_526856_AFO-366_Snip4RFQ.docx\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'GPAT : 0\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'Requirements : see attachment\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + 'MOD Remarks : \n\n\n';
		sewpEmail.Text_Body__c = sewpEmail.Text_Body__c + '**********End of machine readable section**********\n';

		sewpEmail.RFQ__c = null;
		sewpEmail.Has_Attachment__c = false;
		sewpEmail.Outbound_Email_Sent__c = false;
		sewpEmail.RFQ_Modification__c = null;
		sewpEmail.Email_Service_Type__c = 'SEWP V';
		insert sewpEmail;
	}

	@isTest
	static void testCIO()
	{
		setup();

		ReprocessInboundEmail ppc = new ReprocessInboundEmail(new ApexPages.StandardController(cioEmail));
        ppc.reprocess();
	}

	@isTest
	static void testCIOWithError()
	{
		setup();

		ReprocessInboundEmail ppc = new ReprocessInboundEmail(new ApexPages.StandardController(cioEmailError));
        ppc.reprocess();
	}

	@isTest
	static void testSEWP()
	{
		setup();

		ReprocessInboundEmail ppc = new ReprocessInboundEmail(new ApexPages.StandardController(sewpEmail));
        ppc.reprocess();
	}

	@isTest
	static void testFSII()
	{
		setup();

		sewpEmail.Email_Service_Type__c = 'First Source II';
		update sewpEmail;

		ReprocessInboundEmail ppc = new ReprocessInboundEmail(new ApexPages.StandardController(sewpEmail));
        ppc.reprocess();
	}

	@isTest
	static void testNetcents()
	{
		setup();

		sewpEmail.Email_Service_Type__c = 'NETCENTS-2';
		update sewpEmail;

		ReprocessInboundEmail ppc = new ReprocessInboundEmail(new ApexPages.StandardController(sewpEmail));
        ppc.reprocess();
	}

	@isTest
	static void testOpenMarket()
	{
		setup();

		sewpEmail.Email_Service_Type__c = 'OPEN MARKET';
		update sewpEmail;

		ReprocessInboundEmail ppc = new ReprocessInboundEmail(new ApexPages.StandardController(sewpEmail));
        ppc.reprocess();
	}

	@isTest
	static void testSewpDVO()
	{
		setup();

		sewpEmail.Email_Service_Type__c = 'SEWP V (SDVO/SB)';
		update sewpEmail;

		ReprocessInboundEmail ppc = new ReprocessInboundEmail(new ApexPages.StandardController(sewpEmail));
        ppc.reprocess();
	}
}