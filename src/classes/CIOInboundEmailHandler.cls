global with sharing class CIOInboundEmailHandler extends RFQInboundEmailHandler implements Messaging.InboundEmailHandler
{
    global override Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
    {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();

        InboundEmail__c emailRecord = null;

        if(isReprocessing == null || isReprocessing == false)
        {
            try
            {
                emailRecord = EmailService.createEmailRecord(email, getInboundEmailServiceType());
            }
            catch(Exception e)
            {
                result.success = false;
                result.message = 'Inbound Email record could not be created: '+e.getMessage();
                System.debug('Inbound Email record could not be created. The inbound email may have been a duplicate of an existing email.');
                return result; 
            }
        }
        else
        {
            emailRecord = EmailService.getExistingEmailRecord(email);
            if(emailRecord == null)
            {
                result.success = false;
                result.message = 'An Existing Inbound Email record that needed to be reprocessed could not be found. Make sure that an RFQ doesn\'t already exist.';
                System.debug('An Existing Inbound Email record could not be found.');
                return result;
            }
        }

        String emailBody = email.plainTextBody;

        System.debug('**** Email: '+emailBody);
        
        RFQ_RFI__c rfq = null;
        RFQ_Modification__c mod = null;
        String requestId = TextParser.getValueBetween(emailBody,'E-GOS ID: ', '\n');

        Boolean existingRFQ = doesRfqExist(requestId);

        // Check for existing rfq. Mod if it does
        // If cancellation, mark RFQ as cancelled, but still do everything else for the Mod
        if(existingRFQ) 
        {
            try
            {
                rfq = getExistingRFQ(requestId);
                mod = new RFQ_Modification__c();
                mod.RFQ_RFI__c = rfq.Id;
                mod.Mod_Remarks__c = TextParser.getValueBetween(emailBody,'Amendment Description:\n', '\n'); // extra /n is because text is on next line
                mod.Mod_Remarks__c = mod.Mod_Remarks__c+' '+TextParser.getValueBetween(emailBody,'Amendment Summary:\n', '\n');
                
                try
                {
                    mod.Mod_Level__c = Integer.valueOf(TextParser.getValueBetween(emailBody,'Amendment number:\n', '\n'));
                }
                catch(Exception e)
                {
                    mod.Mod_Level__c = 0;
                } 

                mod.Mod_Date__c = System.today();

                String val = TextParser.getValueBetween(emailBody,'Proposals are due by ', 'Eastern Standard Time');
                if(String.isNotBlank(val)) 
                {
                    mod.Mod_Due_Date_Text__c = val;
                }

                mod.Name = requestId+' '+mod.Mod_Level__c;

                insert mod;

                List<Attachment> attachments = EmailService.getAttachmentsInEmail(email, mod.Id);

                if(attachments.size() > 0)
                    insert attachments;
                
                emailRecord.RFQ_Modification__c = mod.Id;
                update emailRecord;

                if(email.Subject.indexOf('Cancellation') > 0)
                {
                    rfq.Status__c = 'Cancelled';
                    update rfq;
                }
            }
            catch(Exception e)
            {
                result.success = false;
                result.message = 'RFQ MOD record could not be created: '+e.getMessage();
                System.debug('RFQ MOD record could not be created: '+e.getMessage());
                return result; 
            }
        }
        else
        {
            try
            {
                rfq = new RFQ_RFI__c();
                rfq.RecordTypeId = getRecordTypeId();
                rfq.Name = requestId;
                rfq.RFQ_RFI_Name__c = requestId;
                rfq.Contract_Type__c = getContractType();
                rfq.Request_Subject__c = TextParser.getValueBetween(emailBody,'Title: ', '\n');
                rfq.Agency__c = TextParser.getValueBetween(emailBody,'Government Customer: ', '\n');
                if(rfq.Agency__c != null && rfq.Agency__c.length() > 100)
                    rfq.Agency__c = rfq.Agency__c.substring(0,100);
                    
                rfq.Requirements__c = TextParser.getValueBetween(emailBody,'Description: ', 'Remarks :');

                try // TODO this one is tricky. Are they always eastern time?
                {
                    String val = TextParser.getValueBetween(emailBody,'Proposals are due by ', 'Eastern Standard Time');
                    if(String.isNotBlank(val)) 
                    {
                        rfq.RFQ_Due_Date_Text__c = val;
                        rfq.RFQ_Due_Date__c = TextParser.getDateTimeSlashes(val);
                    }
                    else
                    {
                        val = TextParser.getValueBetween(emailBody,'All questions must be submitted by ', 'Eastern Standard Time');
                        if(String.isNotBlank(val)) 
                        {
                            rfq.RFQ_Due_Date_Text__c = val;
                            rfq.RFQ_Due_Date__c = TextParser.getDateTimeSlashes(val);
                        }
                    }
                } 
                catch(Exception e) 
                {
                    System.debug('**** Could not get Due Date: '+e);
                }

                insert rfq;

                List<Attachment> attachments = EmailService.getAttachmentsInEmail(email, rfq.Id);

                if(attachments.size() > 0)
                    insert attachments;

                emailRecord.RFQ__c = rfq.Id;
                update emailRecord;
            }
            catch(Exception e)
            {
                result.success = false;
                result.message = 'RFQ record could not be created: '+e.getMessage();
                System.debug('RFQ record could not be created: '+e.getMessage());
                return result; 
            }
        }

        result.success = true;
        result.message = null;
        //if(existingRFQ)
        //    result.message = 'Inbound Email created: '+emailRecord.Id+'\n'+'RFQ MOD Created: '+mod.Id;
        //else
        //    result.message = 'Inbound Email created: '+emailRecord.Id+'\n'+'RFQ Created: '+rfq.Id;

        processEmailForOutboundMessage(emailBody, rfq.Id, emailRecord.Id, getInboundEmailServiceType(), getContractType(), !existingRFQ);

        return result;
    }

	public override Id getRecordTypeId() 
	{
		Id rtId = null;
        try
        {
            RecordType rt = [select Id, Name, DeveloperName, sObjectType from RecordType where sObjectType='RFQ_RFI__c' and IsActive = TRUE and DeveloperName='CIO_CS'];
            rtId = rt.Id;
        }
        catch(Exception e)
        {
            rtId = null;
            System.debug('**** CIOInboundEmailHandler Could not retrieve CIO_CS RecordType.');
        }
        return rtId;
	}

	public override String getContractType()
    {
        return 'CIO-CS';
    }

    public override String getInboundEmailServiceType()
    {
        return 'CIO-CS';
    }
}