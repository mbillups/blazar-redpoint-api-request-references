global with sharing class NETCENTSInboundEmailHandler extends RFQInboundEmailHandler implements Messaging.InboundEmailHandler
{
	global override Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
    {
    	return super.handleInboundEmail(email,envelope);
    }

	public override Id getRecordTypeId() 
	{
		Id rtId = null;
        try
        {
            RecordType rt = [select Id, Name, DeveloperName, sObjectType from RecordType where sObjectType='RFQ_RFI__c' and IsActive = TRUE and DeveloperName='NETCENTS_2'];
            rtId = rt.Id;
        }
        catch(Exception e)
        {
            rtId = null;
            System.debug('**** NETCENTSInboundEmailHandler Could not retrieve NETCENTS_2 RecordType.');
        }
        return rtId;
	}

	public override String getContractType()
    {
        return 'NETCENTS-2';
    }

    public override String getInboundEmailServiceType()
    {
        return 'NETCENTS-2';
    }
}