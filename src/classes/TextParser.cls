public with sharing class TextParser 
{
    // Find a String within a larger text string. Returns true if the word is found.
    public static Boolean findWord(String inText, String word)
    {
        if(String.isBlank(word))
            return false;
            
        String patStr = '('+word.toLowerCase()+')';

        Pattern MyPattern = Pattern.compile(patStr);
        Matcher MyMatcher = MyPattern.matcher(inText.toLowerCase());
        return MyMatcher.find();
    }

    // Find N Strings within a larger text string. The order of the strings is maintained; if a word is found, the next word is then
    // searched for. If a word is not found, none of the following words will be searched for.
    // Returns true for each word that is found. The return list will have false for unfound words.
    public static List<Boolean> findWordsInOrder(String inText, List<String> words)
    {
        List<Boolean> results = new List<Boolean>();
        for(Integer i=0;i<words.size();i++)
            results.add(false);

        for(Integer i=0;i<words.size();i++)
        {
            if(TextParser.findWord(inText, words.get(i)))
            {
                results.set(i, true);
            }
            else
                break;
        }

        return results;
    }

    public static String getValueBetween(String mainStr, String input, String terminator) 
    {
        try 
        {
            String value = mainStr.subStringBetween(input, terminator);
            return value.trim();
        } 
        catch(Exception e) 
        {
            System.debug('**** No value found for: <' + input + '> to <' + terminator + '>');
            return '';
        }
    }

    /*
        This just adds spaces around the term based on the padding value. These value MUST match
        what's in the SearchTerm__c object's picklist for padding
    */
    public static String addPaddingToTerm(String padding, String searchTerm)
    {
        if (padding == 'Leading space')
            return ' ' + searchTerm;
        else if (padding == 'Trailing space')
            return searchTerm + ' ';
        else if (padding == 'Leading and Trailing spaces')
            return ' ' + searchTerm + ' ';

        return searchTerm;
    }

    /*
        This adds quotes around the term. Aspose has a habit of breaking up sentences and returning
        single words. There are no spaces around these words, so padding terms wouldn't be found.
    */
    public static String addAsposePaddingToTerm(String searchTerm)
    {
        return '"'+searchTerm+'"';
    }

    public static String removeAsposePaddingFromTerm(String searchTerm)
    {
        if(searchTerm.startsWith('"') && searchTerm.endsWith('"'))
        {
            String term = searchTerm.replace('"','');
            return term;
        }
        else
            return searchTerm.trim();
    }

    /*
        Returns a Date object from an input string with Known Format: DD-MMM-YYYY
    */
    public static Date getDate(String input) 
    {
        // Define month translation
        Map<String, Integer> months = new Map<String, Integer>{'JAN'=>1, 'FEB'=>2, 'MAR'=>3, 'APR'=>4, 'MAY'=>5, 'JUN'=>6, 'JUL'=>7, 'AUG'=>8, 'SEP'=>9, 'OCT'=>10, 'NOV'=>11, 'DEC'=>12};
        
        //Split date component
        List<String> parts = input.split('-');
        return Date.newInstance(Integer.valueOf(parts[2]), months.get(parts[1]), Integer.valueOf(parts[0]));
        
    }

    /*
        Returns a DateTime object from an input string with Known Format: DD-MMM-YYYY HH:MM
    */
    public static DateTime getDateTime(String input) 
    {
        String dateStr;
        String timeStr;
        
        // Split date and time
        List<String> parts = input.split(' ');
        dateStr = parts[0];
        timeStr = parts[1];
       
        Date d = TextParser.getDate(dateStr);

        // Build time
        List<String> timeParts = timeStr.split(':');
        
        Time t = Time.newInstance(Integer.valueOf(timeParts[0]), Integer.valueOf(timeParts[1]), 0, 0);
        
        //Adjusted for GMT
        //return DateTime.newInstanceGMT(d, t.addHours(6));
        return DateTime.newInstanceGMT(d, t);
    }

    /*
        Returns a Date object from an input string with Known Format: MM/DD/YYYY
    */
    public static Date getDateSlashes(String input) 
    {
        //Split date component
        List<String> parts = input.split('/');
        return Date.newInstance(Integer.valueOf(parts[2]), Integer.valueOf(parts[0]), Integer.valueOf(parts[1]));
    }

    /*
        Returns a DateTime object from an input string with Known Format: MM/DD/YYYY HH:MM AM/PM
    */
    public static DateTime getDateTimeSlashes(String input) 
    {
        String dateStr;
        String timeStr;
        String ampmStr;
        
        // Split date and time
        List<String> parts = input.split(' ');
        dateStr = parts[0];
        timeStr = parts[1];
        ampmStr = parts[2];
       
        Date d = TextParser.getDateSlashes(dateStr);

        // Build time
        List<String> timeParts = timeStr.split(':');
        
        Time t = Time.newInstance(Integer.valueOf(timeParts[0]), Integer.valueOf(timeParts[1]), 0, 0);
        
        //Adjusted for GMT
        return DateTime.newInstance(d, t);
    }
}