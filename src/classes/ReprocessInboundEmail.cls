public with sharing class ReprocessInboundEmail 
{
	private final InboundEmail__c email;
    public String errorMsg{get;set;}
    public String successMsg{get;set;}
    public Boolean isError{get;set;}
    public Boolean showSpinner{get;set;}

    public ReprocessInboundEmail(ApexPages.StandardController stdController) 
    {
        showSpinner = true;
        this.email = [select Id, Name, Subject__c, From_Address__c, Text_Body__c, Has_Attachment__c, Outbound_Email_Sent__c, Email_Service_Type__c from InboundEmail__c where Id =: stdController.getId()];
    }

    public void reprocess()
    {
        if(email.Email_Service_Type__c != null)
        {
            RFQInboundEmailHandler handler = null;
            if(email.Email_Service_Type__c == 'CIO-CS')
            {
                handler = new CIOInboundEmailHandler();
            }
            else if(email.Email_Service_Type__c == 'First Source II')
            {
                handler = new FirstSourceInboundEmailHandler();
            }
            else if(email.Email_Service_Type__c == 'NETCENTS-2')
            {
                handler = new NETCENTSInboundEmailHandler();
            }
            else if(email.Email_Service_Type__c == 'OPEN MARKET')
            {
                handler = new OpenMarketInboundEmailHandler();
            }
            else if(email.Email_Service_Type__c == 'SEWP V (SDVO/SB)')
            {
                handler = new SEWP_SDVOInboundEmailHandler();
            }
            else if(email.Email_Service_Type__c == 'SEWP V')
            {
                handler = new SEWPInboundEmailHandler();
            }

            handler.isReprocessing = true;

            Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
            Messaging.InboundEmail emailObj = getEmailObject();
            Messaging.InboundEmailResult result = handler.handleInboundEmail(emailObj, env);

            if(result.success)
            {
                isError = false;
                errorMsg = null;
                successMsg = result.message;
            }
            else
            {
                isError = true;
                errorMsg = result.message;
                successMsg = null;
            }
        }
        else
        {
            System.debug('**** Can\'t reprocess email. Email Service Type is empty');
            isError = true;
            errorMsg = 'Can\'t reprocess email. Email Service Type is empty';
            successMsg = null;
        }

        showSpinner = false;
    }

    // This just adds the data needed to find the Inbound_Email record. EmailService will use
    // the from address and the subject to find the record.
    private Messaging.InboundEmail getEmailObject()
    {
        Messaging.InboundEmail emailObj = new Messaging.InboundEmail();
        emailObj.fromAddress = email.From_Address__c;
        emailObj.subject = email.Subject__c;
        emailObj.plainTextBody = email.Text_Body__c;

        return emailObj;
    }

}