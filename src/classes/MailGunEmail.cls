public with sharing class MailGunEmail 
{
    public static void sendEmail(EmailWrapper email) 
    {
	    Http h = new Http(); 
	    
	    HttpRequest req = new HttpRequest();
	    HttpResponse res = new HttpResponse();
	    
	    EmailSettings__c settings = EmailSettings__c.getOrgDefaults();

	    req.setHeader('Content-Type', 'multipart/form-data; boundary=' + email.boundary);
	    req.setHeader('Authorization', 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf('api:' + settings.MailGun_Key__c)));
		req.setBody(email.body);
		req.setEndpoint(settings.MailGun_URL__c);
	    req.setMethod('POST');

		if(!Test.isRunningTest())
		{
		    res = h.send(req);
		}
    }
	
	public class EmailWrapper 
	{
		public String boundary;
		public String body = '';
		
		public EmailWrapper() 
		{
			boundary = '---------------RedPoint-' + String.valueOf(Crypto.getRandomLong());
		}

	    public void setFrom(String fromAddress)
	    {
	    	body += '--' + boundary + '\r\n' + 'Content-Disposition: form-data; name="from"' + '\r\n\r\n' +	fromAddress  + '\r\n';
	    }

	    public void setRecipients(List<String> tos)
	    {
	    	for(String t : tos) 
	    	{
		    	body += '--' + boundary + '\r\n' + 'Content-Disposition: form-data; name="to"' + '\r\n\r\n' + t + '\r\n';
	    	}
	    }

	    public void setSubject(String subject)
	    {
	    	body += '--' + boundary + '\r\n' + 'Content-Disposition: form-data; name="subject"' + '\r\n\r\n' + subject  + '\r\n';
	    }

	    public void setText(String text)
	    {
	    	body += '--' + boundary + '\r\n' + 'Content-Disposition: form-data; name="text"' + '\r\n\r\n' +	text  + '\r\n';
	    }

	    public void addAttachment(String filename, Blob content) 
	    {
	    	body += '--' + boundary + '\r\n' + 'Content-Disposition: form-data; name="attachment";' + 
	    		'filename="' + filename + '";\r\nContent-Transfer-Encoding: base64\r\n\r\n' +
				EncodingUtil.base64Encode(content) + '\r\n';
	    }

	    public void setFooter()
	    {
	    	body += '--' + boundary + '--';
	    }
	}
}