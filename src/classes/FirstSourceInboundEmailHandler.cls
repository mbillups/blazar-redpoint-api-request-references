global with sharing class FirstSourceInboundEmailHandler extends RFQInboundEmailHandler implements Messaging.InboundEmailHandler
{
    global override Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
    {
        return super.handleInboundEmail(email,envelope);
    }

	public override Id getRecordTypeId() 
	{
		Id rtId = null;
        try
        {
            RecordType rt = [select Id, Name, DeveloperName, sObjectType from RecordType where sObjectType='RFQ_RFI__c' and IsActive = TRUE and DeveloperName='First_Source_II'];
            rtId = rt.Id;
        }
        catch(Exception e)
        {
            rtId = null;
            System.debug('**** FirstSourceInboundEmailHandler Could not retrieve First_Source_II RecordType.');
        }
        return rtId;
	}

	public override String getContractType()
    {
        return 'First Source II';
    }

    public override String getInboundEmailServiceType()
    {
        return 'First Source II';
    }
}