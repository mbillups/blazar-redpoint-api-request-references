global virtual class RFQInboundEmailHandler implements Messaging.InboundEmailHandler 
{
    public Boolean isReprocessing{get;set;}

    global virtual Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
    {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();

        InboundEmail__c emailRecord = null;

        if(isReprocessing == null || isReprocessing == false)
        {
            try
            {
                emailRecord = EmailService.createEmailRecord(email, getInboundEmailServiceType());
            }
            catch(Exception e)
            {
                result.success = false;
                result.message = 'Inbound Email record could not be created. The inbound email may have been a duplicate of an existing email.';
                System.debug('Inbound Email record could not be created: '+e.getMessage());
                return result; 
            }
        }
        else
        {
            emailRecord = EmailService.getExistingEmailRecord(email);
            if(emailRecord == null)
            {
                result.success = false;
                result.message = 'An Existing Inbound Email record that needed to be reprocessed could not be found. Make sure that an RFQ doesn\'t already exist.';
                System.debug('An Existing Inbound Email record could not be found.');
                return result;
            }
        }
        
        RFQ_RFI__c rfq = null;
        RFQ_Modification__c mod = null;
        String requestId = getRequestId(email.plainTextBody);

        Boolean existingRFQ = doesRfqExist(requestId);

        System.debug('**** Request ID: '+requestId+' exists: '+existingRFQ);

        // Check for existing rfq. Mod if it does
        // If cancellation, mark RFQ as cancelled, but still do everything else for the Mod
        if(existingRFQ) 
        {
            try
            {
                rfq = getExistingRFQ(requestId);
                mod = new RFQ_Modification__c(); 
                mod.RFQ_RFI__c = rfq.Id;
                mod.Mod_Remarks__c = TextParser.getValueBetween(email.plainTextBody,'MOD Remarks :', '****');

                try
                {
                    mod.Mod_Level__c = Double.valueOf(TextParser.getValueBetween(email.plainTextBody,'Mod Level :', '\n'));
                }
                catch(Exception e)
                {
                   // If we already have an existing RFQ and there is no Mod level or Mod Level is 0, we need to abort the processing. This is a duplicate email
                   // and it doesn't need to be processed again.
                    if(isReprocessing == null || isReprocessing == false)
                    {
                        result.success = false;
                        result.message = 'The inbound email was determined to be a duplicate of an already processed email. No further processing will occur';
                        return result; 
                    }
                } 

                List<RFQ_Modification__c> existingMods = [select Id, Name, Mod_Level__c from RFQ_Modification__c where RFQ_RFI__c =: rfq.Id];
                if(existingMods != null && existingMods.size() > 0)
                {
                    Double maxModForExisting = -1;
                    for(RFQ_Modification__c existingMod : existingMods)
                    {
                        Integer existingLevelInt = (Integer)existingMod.Mod_Level__c;
                        if(existingLevelInt == (Integer)mod.Mod_Level__c)
                        {
                            maxModForExisting = Math.max(maxModForExisting, existingMod.Mod_Level__c);
                        }
                    }

                    if(maxModForExisting >= 0) // we have an existing mod at this level
                    {
                        for(RFQ_Modification__c existingMod : existingMods)
                        {
                            Integer existingLevelInt = (Integer)existingMod.Mod_Level__c;
                            
                            if(existingLevelInt == (Integer)mod.Mod_Level__c)
                            {
                                Double dec = maxModForExisting - (Double)existingLevelInt;
                                dec = dec + .1;
                                mod.Mod_Level__c = mod.Mod_Level__c + dec;
                                break;
                            }
                        }
                    }
                }

                String md = TextParser.getValueBetween(email.plainTextBody,'Mod Date :', '\n');
                try 
                {
                    if(String.isNotBlank(md)) 
                        mod.Mod_Date__c = TextParser.getDate(md);
                } 
                catch(Exception e) 
                {
                    System.debug('**** Could not get Request Date: '+e);
                }

                String rp = TextParser.getValueBetween(email.plainTextBody,'Reply by Date :', '\n');
                try 
                {
                    if(String.isNotBlank(rp)) 
                    {
                        mod.Mod_Due_Date__c = TextParser.getDateTime(rp);
                        mod.Mod_Due_Date_Text__c = rp;
                    }
                } 
                catch(Exception e) 
                {
                    System.debug('**** Could not get Reply Date: '+e);
                }

                mod.Name = requestId+' '+mod.Mod_Level__c.setScale(1);

                insert mod;

                List<Attachment> attachments = EmailService.getAttachmentsInEmail(email, mod.Id);

                if(attachments.size() > 0)
                    insert attachments;
                
                emailRecord.RFQ_Modification__c = mod.Id;
                update emailRecord;

                if(email.Subject.indexOf('Cancellation') > 0)
                {
                    rfq.Status__c = 'Cancelled';
                    update rfq;
                }
            }
            catch(Exception e)
            {
                result.success = false;
                result.message = 'RFQ MOD record could not be created: '+e.getMessage();
                System.debug('RFQ MOD record could not be created: '+e.getMessage());
                return result; 
            }
        }
        else
        {
            try
            {
                rfq = new RFQ_RFI__c();
                rfq.RecordTypeId = getRecordTypeId();
                rfq.Name = requestId;
                rfq.RFQ_RFI_Name__c = requestId;
                rfq.Contract_Type__c = getContractType();
                rfq.Request_Subject__c = TextParser.getValueBetween(email.plainTextBody,'Subject :', '\n');
                rfq.First_Name__c = TextParser.getValueBetween(email.plainTextBody,'First Name :', '\n');
                rfq.POC_Last_Name__c = TextParser.getValueBetween(email.plainTextBody,'Last Name :', '\n');
                rfq.Status__c = 'Active';
                
                rfq.POC_Email__c = TextParser.getValueBetween(email.plainTextBody,'Email :', '\n');
                if(rfq.POC_Email__c != null && rfq.POC_Email__c.indexOf('<mailto')>0)
                {
                    rfq.POC_Email__c = TextParser.getValueBetween(email.plainTextBody,'<mailto:', '>');
                }

                rfq.POC_Phone__c = TextParser.getValueBetween(email.plainTextBody,'Phone Number :', '\n');
                rfq.Agency__c = TextParser.getValueBetween(email.plainTextBody,'Agency :', '\n');
                rfq.City__c = TextParser.getValueBetween(email.plainTextBody,'City :', '\n');
                rfq.State__c = TextParser.getValueBetween(email.plainTextBody,'State :', '\n');
                String zip = TextParser.getValueBetween(email.plainTextBody,'Zip :','\n');

                if(zip != 'none supplied') 
                    rfq.Zip_Code__c = TextParser.getValueBetween(email.plainTextBody,'Zip :', '\n');

                String conus = TextParser.getValueBetween(email.plainTextBody,'CONUS :','\n');

                if(conus == 'Contiguous United States') 
                    rfq.CONUS__c = true;
                else
                    rfq.CONUS__c = false;

                rfq.Requirements__c = TextParser.getValueBetween(email.plainTextBody,'Requirements :', 'Remarks :');

                try 
                {
                    String val = TextParser.getValueBetween(email.plainTextBody,'Request Date :', '\n');
                    if(String.isNotBlank(val)) 
                        rfq.Request_Date__c = TextParser.getDate(val);
                } 
                catch(Exception e) 
                {
                    System.debug('**** Could not get Request Date: '+e);
                }
                
                try 
                {
                    String val = TextParser.getValueBetween(email.plainTextBody,'Reply by Date :', '\n');
                    if(String.isNotBlank(val))
                    { 
                        rfq.RFQ_Due_Date_Text__c = val;
                        rfq.RFQ_Due_Date__c = TextParser.getDateTime(val);
                    }
                } 
                catch(Exception e) 
                {
                    System.debug('**** Could not get Reply By Date Date: '+e);
                }

                insert rfq;

                List<Attachment> attachments = EmailService.getAttachmentsInEmail(email, rfq.Id);

                if(attachments.size() > 0)
                    insert attachments;

                emailRecord.RFQ__c = rfq.Id;
                update emailRecord;
            }
            catch(Exception e)
            {
                result.success = false;
                result.message = 'RFQ record could not be created: '+e.getMessage();
                System.debug('RFQ record could not be created: '+e.getMessage());
                return result; 
            }
        }

        result.success = true;
        result.message = null;

        processEmailForOutboundMessage(email.plainTextBody, rfq.Id, emailRecord.Id, getInboundEmailServiceType(), getContractType(), !existingRFQ);

        return result;
    }

    public virtual Id getRecordTypeId()
    {
        Id rtId = null;
        try
        {
            RecordType rt = [select Id, Name, DeveloperName, sObjectType from RecordType where sObjectType='RFQ_RFI__c' and IsActive = TRUE and DeveloperName='OPEN_MARKET'];
            rtId = rt.Id;
        }
        catch(Exception e)
        {
            rtId = null;
            System.debug('**** RFQInboundEmailHandler Could not retrieve RFQ RecordType.');
        }
        return rtId;
    }

    public virtual String getContractType()
    {
        return 'SEWP V';
    }

    public virtual String getInboundEmailServiceType()
    {
        return 'SEWP V';
    }

    public String getRequestId(String emailBody) 
    {
        String rid = TextParser.getValueBetween(emailBody,'Request Seq:', '\n');

        if(rid == '') 
            rid = TextParser.getValueBetween(emailBody,'Request seq#:', '\n');

        if(rid == '') 
            rid = TextParser.getValueBetween(emailBody,'Request ID# :', '\n');

        return rid;
    }

    public Boolean doesRfqExist(String reqId) 
    {
        try 
        {
            RFQ_RFI__c rfq = [SELECT Name FROM RFQ_RFI__c WHERE Name = :reqId];
            return true;
        } 
        catch(QueryException e) 
        {
            return false;
        }
    }

    public RFQ_RFI__c getExistingRFQ(String name)
    {
        try 
        {
            RFQ_RFI__c rfq = [SELECT Name, Id, Contract_Type__c FROM RFQ_RFI__c WHERE Name = :name];
            return rfq;
        } 
        catch(QueryException e) 
        {
            return null;
        }
    }

    @future(callout=true limits='3xHeap')
    public static void processEmailForOutboundMessage(String email, Id rfqId, Id emailRecordId, String inboundEmailType, String contractType, Boolean isNew)
    {
        System.debug('**** RFQ ID: '+rfqId);
        System.debug('**** InboundEmail ID: '+emailRecordId);

        List<RoutingRule__c> rules = [SELECT Id, Name, IsActive__c, TertiaryTerm__r.Name, TertiaryTerm__r.TermPadding__c, SecondaryTerm__r.Name,
                SecondaryTerm__r.TermPadding__c, PrimaryTerm__r.Name, PrimaryTerm__r.TermPadding__c,EmailServiceType__c,
                (SELECT Contact__r.Email, Contact__r.AccountId FROM Outbound_Recipients__r) FROM RoutingRule__c where IsActive__c=true and
                EmailServiceType__c includes ('All',:inboundEmailType)];

        List<Account_Request__c> acctReqs = new List<Account_Request__c>();

        EmailSettings__c settings = EmailSettings__c.getOrgDefaults();

        Map<String, Set<String>> emailTermsMap = new Map<String, Set<String>>();

        if(rules != null && rules.size() > 0)
        {
            RFQInboundEmailHandler.checkStringForRules(email, rules, emailTermsMap, acctReqs, rfqId, false);

            for(Attachment att : [SELECT Name, Body, ParentId, ContentType FROM Attachment WHERE ParentId = :emailRecordId]) 
            {
                if(EmailService.getFileExtension(att.Name) == 'png' || EmailService.getFileExtension(att.Name) == 'jpg' ||
                    EmailService.getFileExtension(att.Name) == 'jpeg')
                    continue; // no point in trying to do anything with image files.

                RFQInboundEmailHandler.checkStringForRules(att.Name, rules, emailTermsMap, acctReqs, rfqId, false); // check name for string
                if(EmailService.apexSupportedAttachmentTypes.contains(EmailService.getFileExtension(att.Name)))
                {
                    RFQInboundEmailHandler.checkStringForRules(att.Body.toString(), rules, emailTermsMap, acctReqs, rfqId, false);
                }
                else if(settings.Use_Aspose__c)
                {
                    // Aspose doesn't like spaces in the filenames. Replace with underscores
                    String attName = att.Name.replace(' ','_');

                    Boolean success = false;
                    try
                    {
                        success = AsposeService.uploadDocument(att.Body, attName);
                    }
                    catch(Exception e)
                    {
                        System.debug('**** Aspose upload failed: '+e.getMessage());
                    }

                    if(success)
                    {
                        try
                        {
                            // Commented code is for when each token is it's own string to be parsed. Caused CPU limit problems, so tabled for now.
                            List<String> jsonStrs = AsposeService.getJSONOfUploadedDocument(attName, EmailService.getFileExtension(attName), AsposeService.getFolderName());
                            for(String json : jsonStrs)
                                RFQInboundEmailHandler.checkStringForRules(json, rules, emailTermsMap, acctReqs, rfqId, true);
                            //String json = AsposeService.getJSONOfUploadedDocument(attName, EmailService.getFileExtension(attName), AsposeService.getFolderName());
                            //RFQInboundEmailHandler.checkStringForRules(json, rules, emailTermsMap, acctReqs, rfqId, true);
                        }
                        catch(Exception e)
                        {
                            System.debug('**** Aspose download of attachment '+attName+' failed: '+e.getMessage());
                        }
                    }
                }
            }

            InboundEmail__c emailRecord = [select Id, Name from InboundEmail__c where Id =: emailRecordId];
            if(!emailTermsMap.isEmpty())
            {
                List<Messaging.SingleEmailMessage> emails = getRecipientEmails(emailTermsMap, rfqId, email, contractType, isNew);

                // Send in attachments separately. Email Service will attach them to the outbound emails. Keeps them from bogging up the heap
                List<Attachment> attachments = [SELECT Name, Body, ParentId, ContentType FROM Attachment WHERE ParentId = :emailRecordId];
                
                String sendResult = EmailService.sendOutboundEmails(emails, attachments);

                if(sendResult == 'Success')
                {
                    emailRecord.Outbound_Email_Sent__c = true;
                    emailRecord.Outbound_Email_Error__c = 'Email Sent';
                }
                else
                {
                    emailRecord.Outbound_Email_Sent__c = false;
                    emailRecord.Outbound_Email_Error__c = 'Error when Sending Email';
                }
            }
            else
            {
                emailRecord.Outbound_Email_Sent__c = false;
                emailRecord.Outbound_Email_Error__c = 'No Terms Found';
            }

            update emailRecord;

            if(acctReqs.size() > 0)
                insert acctReqs;
        }
    }

    public static void checkStringForRules(String email, List<RoutingRule__c> rules, Map<String, Set<String>> emailTermsMap, List<Account_Request__c> acctReqs, Id rfqId,
        Boolean isFromAspose)
    {

        // Aspose returns their URL in their JSON text. Remove it so it doesn't get checked for matching terms
        if(isFromAspose)
            email = email.replace('http://api.aspose.cloud','');

        for(RoutingRule__c rule : rules)
        {
            if(rule.Outbound_Recipients__r == null || rule.Outbound_Recipients__r.size()<=0)
            {
                System.debug('**** There are no Outbound Recipients for Rule '+rule.Name+'. No need to parse.');
                continue;
            }

            List<String> allTerms = new List<String>();
            List<String> asposeTerms = new List<String>();

            if(rule.PrimaryTerm__r != null)
            {
                String primaryTerm = TextParser.addPaddingToTerm(rule.PrimaryTerm__r.TermPadding__c, rule.PrimaryTerm__r.Name);
                allTerms.add(primaryTerm);

                if(isFromAspose)
                {
                    String primAsp = TextParser.addAsposePaddingToTerm(rule.PrimaryTerm__r.Name); 
                    asposeTerms.add(primAsp);
                }

                if(rule.SecondaryTerm__r != null) // tertiary is inside this so we prevent having a tertiary without a secondary
                {
                    String secondaryTerm = TextParser.addPaddingToTerm(rule.SecondaryTerm__r.TermPadding__c, rule.SecondaryTerm__r.Name);
                    allTerms.add(secondaryTerm);

                    if(isFromAspose)
                    {
                        String secAsp = TextParser.addAsposePaddingToTerm(rule.SecondaryTerm__r.Name); 
                        asposeTerms.add(secAsp);
                    }

                    if(rule.TertiaryTerm__r != null)
                    {
                        String tertiaryTerm = TextParser.addPaddingToTerm(rule.TertiaryTerm__r.TermPadding__c, rule.TertiaryTerm__r.Name);
                        allTerms.add(tertiaryTerm);
                        if(isFromAspose)
                        {
                            String terAsp = TextParser.addAsposePaddingToTerm(rule.TertiaryTerm__r.Name); 
                            asposeTerms.add(terAsp);
                        }
                    }
                }
            }

            if(allTerms.size() > 0)
            {
                getResultsFromTextParser(email, allTerms, rule, emailTermsMap, acctReqs, rfqId);
            }

            if(isFromAspose && asposeTerms.size() > 0)
            {
                getResultsFromTextParser(email, asposeTerms, rule, emailTermsMap, acctReqs, rfqId);
            }

            if(allTerms.size() <= 0 && asposeTerms.size() <= 0)
            {
                System.debug('**** There are no Search Terms for Rule '+rule.Name+'. No need to parse.');
                continue;
            }
        }
    }

    private static List<Messaging.SingleEmailMessage> getRecipientEmails(Map<String, Set<String>> emailTermsMap, Id rfqId, String origEmail, String contractType, Boolean isNew)
    {
        RFQ_RFI__c rfq = [select Id, Name, Agency__c from RFQ_RFI__c where Id =: rfqId];
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        for(String key : emailTermsMap.keySet())
        {
            System.debug('**** Email: '+key);
            System.debug('**** Terms: '+emailTermsMap.get(key));

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String emailType = 'New Request';
            if(!isNew)
                emailType = 'MOD/Update to Request';

            String subject = contractType+' '+rfq.Name+' '+emailType+' ('+rfq.Agency__c+')';
            mail.setSubject(subject);
            //mail.setReplyTo(settings.getReplyTo());
            mail.setWhatId(rfqId);
            mail.setSaveAsActivity(true);
            mail.setToAddresses(new List<String>{key});

            String mailBody = '********* Matched Terms *********' + '\r\n';
            for(String term : emailTermsMap.get(key))
                mailBody += term + '\r\n';

            Boolean useHtml = false;
            if(origEmail.indexOf('<html>') >= 0 || origEmail.indexOf('<HTML>') >= 0)
                useHtml = true;

            if(!useHtml)
            {
                mail.plainTextBody = mailBody + '\r\n********* Original Email *********\r\n';
                mail.plainTextBody += origEmail;
            }
            else
            {
                mail.htmlBody = mailBody + '\r\n********* Original Email *********\r\n';
                mail.htmlBody += origEmail;
            }

            // attachments are handled by the EmailService so they don't bog-up the heap

            emails.add(mail);
        }

        return emails;
    }

    private static void getResultsFromTextParser(String email, List<String> allTerms, RoutingRule__c rule, Map<String, Set<String>> emailTermsMap,
        List<Account_Request__c> acctReqs, Id rfqId)
    {
        List<Boolean> results = TextParser.findWordsInOrder(email, allTerms);

        // get the terms that were matched. We need to make sure that ALL terms were matched before an email is sent out. If only one term
        // and it matches, great. If two terms and only one matches, bad.
        Set<String> matchedTerms = new Set<String>();
        Integer index=0;
        Boolean matchedAllTerms = true;
        for(Boolean res : results)
        {
            if(res)
            {
                matchedTerms.add(TextParser.removeAsposePaddingFromTerm(allTerms.get(index)));
            }
            else
            {
                matchedAllTerms = false;
            }

            index++;
        }
        if(matchedAllTerms && matchedTerms.size() > 0)
        {
            // Add the recipient emails and the terms to the map
            for(OutboundRecipient__c recip : rule.Outbound_Recipients__r)
            {
                if(emailTermsMap.containsKey(recip.Contact__r.Email)) // email is already in the map. Modify the matched terms
                {
                    Set<String> existingTerms = emailTermsMap.get(recip.Contact__r.Email);
                    
                    for(String term : matchedTerms)
                    {
                        if(!existingTerms.contains(term)) // should prevent the scenario where the email and the attachment both had the term
                        {
                            existingTerms.add(term);
                        }
                    }

                    emailTermsMap.put(recip.Contact__r.Email, existingTerms);
                }
                else
                {
                    // Add a reference to the Account in the RFQ. If the Account is already in the list, don't add it again.
                    Boolean inAcctReqs = false;
                    if(acctReqs.size() > 0)
                    {
                        for(Account_Request__c ar : acctReqs)
                        {
                            if(ar.Account__c == recip.Contact__r.AccountId)
                                inAcctReqs = true;
                        }
                    }

                    if(!inAcctReqs)
                    {
                        Account_Request__c acctReq = new Account_Request__c(Account__c=recip.Contact__r.AccountId, RFQ__c=rfqId);
                        acctReqs.add(acctReq);
                    }

                    Set<String> thisMatchedTerms = new Set<String>();
                    thisMatchedTerms.addAll(matchedTerms);

                    emailTermsMap.put(recip.Contact__r.Email, thisMatchedTerms);
                }
            }
        }
    }
}