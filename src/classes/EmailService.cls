public with sharing class EmailService 
{
	public static final Set<String> apexSupportedAttachmentTypes = new Set<String>{'txt','csv'};

	public static String sendOutboundEmails(List<Messaging.SingleEmailMessage> emails, List<Attachment> attachments) 
	{
		EmailSettings__c settings = EmailSettings__c.getOrgDefaults();
		if(settings.Use_MailGun__c)
		{
			try
			{
				sendViaMailGun(emails, settings.From_Address__c, attachments);
			}
			catch(Exception e)
			{
				System.debug('**** Emails could not be sent via MailGun: '+e.getMessage());
                return e.getMessage();
			}
		}
		else
        {
            try
            {
                for(Messaging.SingleEmailMessage mail : emails)
                {
                    List<Messaging.EmailFileAttachment> efas = new List<Messaging.EmailFileAttachment>();

                    if(attachments != null && attachments.size()>0)
                    {
                        for(Attachment a : attachments) 
                        {
                            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                            efa.setFileName(a.Name);
                            efa.setContentType(a.ContentType);
                            efa.setBody(a.Body);
                            efas.add(efa);
                        }
                        mail.setFileAttachments(efas);
                    }
                }
                
                if(!Test.isRunningTest())
                {
                    Messaging.sendEmail(emails);
                }
            }
            catch(Exception e)
            {
                System.debug('**** Emails could not be sent via Messaging: '+e.getMessage());
                return e.getMessage();
            }
            
        }

        return 'Success';
	}

	public static void sendViaMailGun(List<Messaging.SingleEmailMessage> emails, String fromAddress, List<Attachment> attachments)
	{
		// Check callout limit. Throw Exception if we'll go over
        if(Limits.getCallouts() + emails.size() > Limits.getLimitCallouts())
            throw new EmailServiceException('Callout limit reached, can\'t send via MailGun.');

        for(Messaging.SingleEmailMessage inEmail : emails)
        {
            // Build new email
            MailGunEmail.EmailWrapper email = new MailGunEmail.EmailWrapper();
            email.setFrom(fromAddress);
            email.setRecipients(inEmail.getToAddresses());
            email.setSubject(inEmail.getSubject());

            if(inEmail.getHtmlBody() != null)
                email.setText(inEmail.getHtmlBody());
            else if(inEmail.getPlainTextBody() != null)
                email.setText(inEmail.getPlainTextBody());
            
            if(attachments != null && attachments.size()>0)
            {
	            for(Attachment att : attachments) 
	            {
	                email.addAttachment(att.Name, att.Body);
	            }
	        }

            email.setFooter();
            
            MailGunEmail.sendEmail(email);
        }
	}

	public static InboundEmail__c createEmailRecord(Messaging.InboundEmail email, String serviceType)
    {
    	InboundEmail__c inEmail = new InboundEmail__c();
    	inEmail.From_Address__c = email.fromAddress;

    	inEmail.Has_Attachment__c = false;
    	inEmail.Outbound_Email_Sent__c = false;
    	inEmail.Subject__c = email.subject;
    	inEmail.Text_Body__c = email.plainTextBody;
        if(inEmail.Text_Body__c == null)
            inEmail.Text_Body__c = email.htmlBody;

        inEmail.Email_Service_Type__c = serviceType;
        if(email.binaryAttachments != null || email.textAttachments != null)
            inEmail.Has_Attachment__c = true;

        Double modLevel = EmailService.getMod(inEmail.Text_Body__c);
        String modDate = EmailService.getModDate(inEmail.Text_Body__c);
        String replyDate = EmailService.getReplyDate(inEmail.Text_Body__c);

        String modStr = '';
        if(modLevel != null)
            modStr = ' ' + String.valueOf(modLevel);

        String modDateStr = '';
        if(modDate != null)
            modDateStr = ' ' + modDate;

        String replyDateStr = '';
        if(replyDate != null)
            replyDateStr = ' ' + replyDate;

        inEmail.Unique_Subject__c = email.subject + modStr + modDateStr + replyDateStr;

    	insert inEmail;

    	List<Attachment> attachments = EmailService.getAttachmentsInEmail(email, inEmail.Id);

        if(attachments.size() > 0)
            insert attachments;

    	return inEmail;
    }

    public static InboundEmail__c getExistingEmailRecord(Messaging.InboundEmail email)
    {
        InboundEmail__c existEmail = null;
        try
        {
            existEmail = [select Id, Name, Subject__c, From_Address__c, Text_Body__c, Has_Attachment__c, Outbound_Email_Sent__c, Email_Service_Type__c from InboundEmail__c where 
                Subject__c =: email.subject AND From_Address__c =: email.fromAddress AND RFQ__c = null];
        }
        catch(Exception e)
        {
            System.debug('**** Existing Email cannot be found. Returning null');
        }

        return existEmail;
    }

    public static List<Attachment> getAttachmentsInEmail(Messaging.InboundEmail email, Id parentId)
    {
    	List<Attachment> attachments = new List<Attachment>();
        if(email.binaryAttachments != null)
        {
            for (Integer i = 0;i < email.binaryAttachments.size();i++) 
            {
                Attachment attachment = new Attachment();
                attachment.ParentId = parentId;
                attachment.Name = email.binaryAttachments[i].filename;
                attachment.Body = email.binaryAttachments[i].body;
                attachment.ContentType = email.binaryAttachments[i].mimeTypeSubType;
                attachments.add(attachment);
            }
        }

        if(email.textAttachments != null)
        {
            for (Integer i = 0;i < email.textAttachments.size();i++) 
            {
                Attachment attachment = new Attachment();
                attachment.ParentId = parentId;
                attachment.Name = email.textAttachments[i].filename;
                attachment.Body = Blob.valueOf(email.textAttachments[i].body);
                attachment.ContentType = email.textAttachments[i].mimeTypeSubType;
                attachments.add(attachment);
            }
        }

        return attachments;
    }

    public static String getFileExtension(String fileName)
    {
        try
        {
            if(fileName.contains('.'))
            {
                List<String> splitList = fileName.split('\\.');
                return splitList[splitList.size()-1]; //last string should be the extension
            }
        }
        catch(Exception e)
        {
           System.debug('**** Error trying to determine file extension'+e.getMessage());
        }

        return '';
    }

    public static Double getMod(String emailBody)
    {
        try
        {
            Double modLevel = Double.valueOf(TextParser.getValueBetween(emailBody,'Mod Level :', '\n'));
            if(modLevel == 0)
                throw new NullPointerException(); // let exception handler below deal with this

            return modLevel;
        }
        catch(Exception e)
        {
            return null;
        }
    }

    public static String getModDate(String emailBody)
    {
        try
        {
            String md = TextParser.getValueBetween(emailBody,'Mod Date :', '\n');
            if(md == null)
                throw new NullPointerException(); // let exception handler below deal with this

            return md;
        }
        catch(Exception e)
        {
            return null;
        }
    }

    public static String getReplyDate(String emailBody)
    {
        try
        {
            String md = TextParser.getValueBetween(emailBody,'Reply by Date :', '\n');
            if(md == null)
                throw new NullPointerException(); // let exception handler below deal with this

            return md;
        }
        catch(Exception e)
        {
            return null;
        }
    }

    public class EmailServiceException extends Exception {}
}