@isTest
private class Test_RoutingRuleTrigger
{
	@isTest
	static void testTriggerNoError()
	{
		SearchTerm__c st1 = new SearchTerm__c();
		st1.IsActive__c = true;
		st1.Name = 'test';
		st1.TermPadding__c = 'Leading and Trailing spaces';
		insert st1;

		SearchTerm__c st2 = new SearchTerm__c();
		st2.IsActive__c = true;
		st2.Name = 'test2';
		st2.TermPadding__c = 'Leading and Trailing spaces';
		insert st2;

		RoutingRule__c rule1 = new RoutingRule__c();
		rule1.IsActive__c = true;
		rule1.PrimaryTerm__c = st1.Id;
		rule1.EmailServiceType__c = 'All';
		insert rule1;

		RoutingRule__c rule2 = new RoutingRule__c();
		rule2.IsActive__c = true;
		rule2.PrimaryTerm__c = st2.Id;
		rule2.EmailServiceType__c = 'All';

		try
		{
			insert rule2;
		}
		catch(Exception ignore)
		{}

		System.assert(rule2.Id != null);

	}

	@isTest
	static void testTriggerNoErrorWithDiffServiceTypes()
	{
		SearchTerm__c st1 = new SearchTerm__c();
		st1.IsActive__c = true;
		st1.Name = 'test';
		st1.TermPadding__c = 'Leading and Trailing spaces';
		insert st1;

		RoutingRule__c rule1 = new RoutingRule__c();
		rule1.IsActive__c = true;
		rule1.PrimaryTerm__c = st1.Id;
		rule1.EmailServiceType__c = 'All';
		insert rule1;

		RoutingRule__c rule2 = new RoutingRule__c();
		rule2.IsActive__c = true;
		rule2.PrimaryTerm__c = st1.Id;
		rule2.EmailServiceType__c = 'All; CIO-CS';

		try
		{
			insert rule2;
		}
		catch(Exception ignore)
		{}

		System.assert(rule2.Id != null);

	}

	@isTest
	static void testTriggerError()
	{
		SearchTerm__c st1 = new SearchTerm__c();
		st1.IsActive__c = true;
		st1.Name = 'test';
		st1.TermPadding__c = 'Leading and Trailing spaces';
		insert st1;

		RoutingRule__c rule1 = new RoutingRule__c();
		rule1.IsActive__c = true;
		rule1.PrimaryTerm__c = st1.Id;
		rule1.EmailServiceType__c = 'All';
		insert rule1;

		RoutingRule__c rule2 = new RoutingRule__c();
		rule2.IsActive__c = true;
		rule2.PrimaryTerm__c = st1.Id;
		rule2.EmailServiceType__c = 'All';

		try
		{
			insert rule2;
		}
		catch(Exception ignore)
		{}

		System.assert(rule2.Id == null);

	}
}