global with sharing class OpenMarketInboundEmailHandler extends RFQInboundEmailHandler  implements Messaging.InboundEmailHandler
{
    global override Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
    {
        return super.handleInboundEmail(email,envelope);
    }

	public override Id getRecordTypeId() 
	{
		Id rtId = null;
        try
        {
            RecordType rt = [select Id, Name, DeveloperName, sObjectType from RecordType where sObjectType='RFQ_RFI__c' and IsActive = TRUE and DeveloperName='OPEN_MARKET'];
            rtId = rt.Id;
        }
        catch(Exception e)
        {
            rtId = null;
            System.debug('**** OpenMarketInboundEmailHandler Could not retrieve OPEN_MARKET RecordType.');
        }
        return rtId;
	}

	public override String getContractType()
    {
        return 'OPEN MARKET';
    }

    public override String getInboundEmailServiceType()
    {
        return 'OPEN MARKET';
    }
}