public with sharing class AsposeService 
{
	public static final String baseEndpoint = 'https://api.aspose.com/v1.1/';

	public static Boolean uploadDocument(Blob body, String documentName) 
	{
		EmailSettings__c settings = EmailSettings__c.getOrgDefaults();

        String requestURI = AsposeService.baseEndpoint + 'storage/file/' + settings.Aspose_Folder_Name__c + '/' + EncodingUtil.urlEncode(documentName, 'UTF-8');

        HttpResponse result = AsposeService.sendRequest('PUT', requestURI, body);

        if(result != null && result.getStatusCode() == 200) 
        {
            return true;
        }
        else 
        {
            throw new AsposeServiceException('Document failed to upload.');
        }
    }

    public static List<String> getJSONOfUploadedDocument(String documentName, String documentType, String folderName)
    {
    	String requestURI = null;
    	if(documentType == 'ocr')
    		requestURI = AsposeService.baseEndpoint + documentType+'/' + EncodingUtil.urlEncode(documentName, 'UTF-8') + '/recognize';
    	else if(documentType == 'docx' || documentType == 'doc')
    		requestURI = AsposeService.baseEndpoint + 'words/' + EncodingUtil.urlEncode(documentName, 'UTF-8') + '/textItems';
    	else if(documentType == 'xlsx' || documentType == 'xls')
    		requestURI = AsposeService.baseEndpoint + 'cells/' + EncodingUtil.urlEncode(documentName, 'UTF-8') + '/textItems';
    	else
    		requestURI = AsposeService.baseEndpoint + documentType+'/' + EncodingUtil.urlEncode(documentName, 'UTF-8') + '/textItems';

        requestURI += '?folder=' + folderName;
        
        HttpResponse result = AsposeService.sendRequest('GET', requestURI, null);

        if(result.getStatusCode() == 404)
            throw new AsposeServiceException('The document: ' + documentName + ' could not be found.');

        System.debug('**** old JSON size: '+result.getBody().length());

        String retText = result.getBody();

        List<String> retStrs = new List<String>();

        if(retText.length() > 500000)
        {
            Integer index = 0;
            Integer startIndex = index*500000;
            Integer endIndex = startIndex+500000;
           
            while((startIndex+500000) < retText.length()) 
            {
                String nextChunk = retText.substring(startIndex,endIndex);
                System.debug('**** chunk: '+nextChunk);
                retStrs.add(nextChunk); 
                index++;

                startIndex = index*500000;
                endIndex = startIndex+500000;
            }

            retStrs.add(retText.substring(startIndex)); // last of it
        }
        else
        {
            retStrs.add(retText);
        }

        System.debug('**** array size: '+retStrs.size());

        //return AsposeService.parseJSONBody(result.getBody()); this was for return all token individually. That's been tabled for now
        return retStrs;
    }

    public static HttpResponse sendRequest(String requestType, String requestURI, Blob body)
    {
    	EmailSettings__c settings = EmailSettings__c.getOrgDefaults();
    	String signedRequestURI = null;

    	try
    	{
    		signedRequestURI = AsposeService.getSignedRequest(requestURI, settings.Aspose_ID__c, settings.Aspose_Key__c);
    	}
    	catch(Exception e)
    	{
    		throw e;
    	}

    	try 
    	{
            HttpRequest request = new HttpRequest();
            Integer len = 0;
            if (body != null) 
            {
                request.setBodyAsBlob(body);
            }

            request.setEndpoint(signedRequestURI);
            request.setMethod(requestType);
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Accept', 'application/json');

            Http http = new Http();
            HttpResponse res = null;

            if(!Test.isRunningTest())
            	res = http.send(request);
            else
            {
               	res = new HttpResponse();
               	res.setStatusCode(200);
               	res.setStatus('SUCCESS');
            }
            
            return res;
        } 
        catch (Exception ex) 
        {
           	System.debug('HTTP ERROR' + ex.getMessage());
            System.debug(ex.getStackTraceString());
            throw new AsposeServiceException('Aspose Callout Error. ' + ex.getMessage());
        }

        return null;
    }

    public static String getFolderName()
    {
    	EmailSettings__c settings = EmailSettings__c.getOrgDefaults();
    	return settings.Aspose_Folder_Name__c;
    }

    private static String getSignedRequest(String data, String appId, String appKey) 
    {
        String SHA = 'HmacSHA1';
        try 
        {
            data = data.replace(' ', '%20');

            URL url = new URL(data);
            String path = url.getPath();
            path = path.replace(' ', '%20');

            // Remove final slash here as it can be added automatically.
            path = path.removeEnd('/');

            String filePart = url.getPath() + (url.getQuery() == null ? '?' : '?' + url.getQuery()) + '&appSID=' + appId;
            url = new URL(url.getProtocol(), url.getHost(), url.getPort(), filePart);

            // compute the hmac on input data bytes
            Blob mac = Crypto.generateMac(SHA, Blob.valueOf(url.toExternalForm()), Blob.valueOf(appKey));
            String base64 = EncodingUtil.base64Encode(mac);
            // Remove invalid symbols.
            String result = base64.substring(0, base64.length() - 1);

            result = EncodingUtil.urlEncode(result, 'UTF-8');

            String encodedUrl = url.toExternalForm() + '&signature=' + result;

            return encodedUrl;
        } 
        catch (Exception ex) 
        {
            System.debug('HTTP ERROR' + ex.getMessage());
            System.debug(ex.getStackTraceString());
            throw new AsposeServiceException('AsposeService could not generated signed request: ' + ex.getMessage());
        }
    }

    //public static List<String> parseJSONBody(String body)
    //{
    //    JSONParser parser = JSON.createParser(body);

    //    List<String> retStrs = new List<String>();

    //    parser.nextToken();
    //    parser.nextToken();
    //    parser.nextToken();
    //    parser.nextToken();
    //    while (parser.nextValue() != null) 
    //    {
    //        if (parser.getCurrentToken() == JSONToken.VALUE_STRING && parser.getCurrentName() == 'Text')
    //        {
    //            String text = parser.getText().trim();
    //            if(text.length() > 1)
    //            {
    //                retStrs.add(parser.getText());
    //            }
    //            else
    //                System.debug('**** Ignoring Token: <'+text+'>');
    //        }
    //    }

    //    return retStrs;
    //}

    public class AsposeServiceException extends Exception {}
}