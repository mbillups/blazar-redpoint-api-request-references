@isTest
private class TestEmailService
{
	static Messaging.InboundEmail email;
	static Messaging.InboundEmail modEmail;

	static void setup()
	{
		List<Messaging.InboundEmail.TextAttachment> textAtts = new List<Messaging.InboundEmail.TextAttachment>();
		Messaging.InboundEmail.TextAttachment ta = new Messaging.InboundEmail.TextAttachment();
		ta.body = 'Text Attachment';
		ta.FileName = 'Text Attachment FileName.csv';
		textAtts.add(ta);

		email = new Messaging.InboundEmail();
		email.binaryAttachments = null;
		email.textAttachments = textAtts;
		email.fromAddress = 'joe@foo.com';
		email.subject = 'Test Email';
		email.plainTextBody = 'This email is being sent to you from the SEWP V QRT Tool.\n\n';
		email.plainTextBody = email.plainTextBody + 'Email Type : NEW Request\n\n';
		email.plainTextBody = email.plainTextBody + 'Request Type : Request For Quote\n';
		email.plainTextBody = email.plainTextBody + 'SEWP Version : V\n';
		email.plainTextBody = email.plainTextBody + 'Request ID# : 49422\n';
		email.plainTextBody = email.plainTextBody + 'Agency ID : AFO-336_ARC\n';
		email.plainTextBody = email.plainTextBody + 'Subject : Open Splice and Vortex Dev. Lic. Support (NEW, not renewal)\n';
		email.plainTextBody = email.plainTextBody + 'Request Date : 21-DEC-2016\n';
		email.plainTextBody = email.plainTextBody + 'Reply by Date : 29-DEC-2016 23:59\n';
		email.plainTextBody = email.plainTextBody + 'Mod Level : 0\n';
		email.plainTextBody = email.plainTextBody + 'Mod Date : 21-DEC-2016\n';
		email.plainTextBody = email.plainTextBody + 'First Name : SERVI\n';
		email.plainTextBody = email.plainTextBody + 'Last Name : NASA-AMES\n';
		email.plainTextBody = email.plainTextBody + 'Phone Number : (650) 604-XXXX\n';
		email.plainTextBody = email.plainTextBody + 'Email : arc-dl-servi@mail.nasa.gov\n';
		email.plainTextBody = email.plainTextBody + 'Agency : National Aeronautics and Space Administration\n';
		email.plainTextBody = email.plainTextBody + 'Address1 : none supplied\n';
		email.plainTextBody = email.plainTextBody + 'Address2 : none supplied\n';
		email.plainTextBody = email.plainTextBody + 'Address3 : \n';
		email.plainTextBody = email.plainTextBody + 'Address4 : \n';
		email.plainTextBody = email.plainTextBody + 'City : none supplied\n';
		email.plainTextBody = email.plainTextBody + 'State : CA\n';
		email.plainTextBody = email.plainTextBody + 'Zip : none supplied\n';
		email.plainTextBody = email.plainTextBody + 'CONUS : Contiguous United States\n';
		email.plainTextBody = email.plainTextBody + 'Agency : National Aeronautics and Space Administration\n';
		email.plainTextBody = email.plainTextBody + 'Attachments included : Y\n';
		email.plainTextBody = email.plainTextBody + 'Attachments online : Y\n';
		email.plainTextBody = email.plainTextBody + 'Attachments : rfq_49422_526856_AFO-366_Snip4RFQ.docx\n';
		email.plainTextBody = email.plainTextBody + 'GPAT : 0\n';
		email.plainTextBody = email.plainTextBody + 'Requirements : see attachment\n';
		email.plainTextBody = email.plainTextBody + 'MOD Remarks : \n\n\n';
		email.plainTextBody = email.plainTextBody + '**********End of machine readable section**********\n';

	}

	@isTest
	static void testCreateEmail()
	{
		setup();

		InboundEmail__c emailRec = EmailService.createEmailRecord(email, 'SEWP V');
		System.assert(emailRec.Id != null);

		List<Attachment> atts = EmailService.getAttachmentsInEmail(email, emailRec.Id);
		System.assert(atts != null);

		List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setSubject('Test Email');
        mail.setSaveAsActivity(true);
        mail.setToAddresses(new List<String>{'joe@schmo.com'});
        mail.plainTextBody = 'test body';
        emails.add(mail);

  		EmailService.sendOutboundEmails(emails, atts);
	}

	@isTest
	static void testSendViaMailGun()
	{
		setup();

		InboundEmail__c emailRec = EmailService.createEmailRecord(email, 'SEWP V');
		List<Attachment> atts = EmailService.getAttachmentsInEmail(email, emailRec.Id);

		List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setSubject('Test Email');
        mail.setSaveAsActivity(true);
        mail.setToAddresses(new List<String>{'joe@schmo.com'});
        mail.plainTextBody = 'test body';

        emails.add(mail);

        EmailSettings__c setting = new EmailSettings__c();
		setting.Use_MailGun__c = true;
		insert setting;

  		EmailService.sendOutboundEmails(emails, atts);
	}

	@isTest
	static void testExtension()
	{
		String ext = EmailService.getFileExtension('Filename_date.csv');
		System.assert(ext == 'csv');

		String empty = EmailService.getFileExtension(null);
		System.assert(empty == '');
	}
}