@isTest
private class TestCIOInboundEmailHandler
{
	static Messaging.InboundEmail email;
	static Messaging.InboundEmail modEmail;

	static void setup()
	{
		List<Messaging.InboundEmail.TextAttachment> textAtts = new List<Messaging.InboundEmail.TextAttachment>();
		Messaging.InboundEmail.TextAttachment ta = new Messaging.InboundEmail.TextAttachment();
		ta.body = 'Text Attachment';
		ta.FileName = 'Text Attachment FileName.csv';
		textAtts.add(ta);

		email = new Messaging.InboundEmail();
		email.binaryAttachments = null;
		email.textAttachments = textAtts;
		email.fromAddress = 'joe@foo.com';
		email.subject = 'RFQ CS-38204-SB Questions and Answers Released';
		email.htmlBody = 'Questions and answers for the order RFQ CS-38204-SB has been released\n\n';
		email.htmlBody = email.htmlBody + 'Please visit e-GOS at https://cio.egos.nih.gov for detailed information.\n\n';
		email.htmlBody = email.htmlBody + 'E-GOS ID: CS-38204-SB\n';
		email.htmlBody = email.htmlBody + 'Request: RFQ\n';
		email.htmlBody = email.htmlBody + 'Title: EXXACT Spectrum TXR410-0032R Graphics Processing Unit (GPU)\n';
		email.htmlBody = email.htmlBody + 'Description: 1 EACH - Spectrum TXR410-0032R with the following minimum specifications:256GB DDR44 x GTX 1080 PascalNIVIDIA DIGITSHardware?4U Convertible\n';
		email.htmlBody = email.htmlBody + 'Government Customer: Department of Health and Human Services, NIH, CC\n\n';
		email.htmlBody = email.htmlBody + 'All questions must be submitted by 12/21/2016 7:00 PM Eastern Standard Time\n\n';
		email.htmlBody = email.htmlBody + 'You can view them by logging into e-GOS and clicking on the Documents tab for RFQ CS-38204-SB .\n\n';

		modEmail = new Messaging.InboundEmail();
		modEmail.binaryAttachments = null;
		modEmail.textAttachments = textAtts;
		modEmail.fromAddress = 'joe@foo.com';
		modEmail.subject = 'Amendment for RFQ CS-38204-SB Released.';
		modEmail.htmlBody = 'An amendment for RFQ CS-38204-SB has been released.\n\n';
		modEmail.htmlBody = modEmail.htmlBody + 'Please visit e-GOS at https://cio.egos.nih.gov for detailed information.\n\n';
		modEmail.htmlBody = modEmail.htmlBody + 'E-GOS ID: CS-38204-SB\n';
		modEmail.htmlBody = modEmail.htmlBody + 'Request: RFQ\n';
		modEmail.htmlBody = modEmail.htmlBody + 'Title: EXXACT Spectrum TXR410-0032R Graphics Processing Unit (GPU)\n';
		modEmail.htmlBody = modEmail.htmlBody + 'Description: 1 EACH - Spectrum TXR410-0032R with the following minimum specifications:256GB DDR44 x GTX 1080 PascalNIVIDIA DIGITSHardware?4U Convertible\n';
		modEmail.htmlBody = modEmail.htmlBody + 'Government Customer: Department of Health and Human Services, NIH, CC\n\n';
		modEmail.htmlBody = modEmail.htmlBody + 'Amendment Description:\n\n';
		modEmail.htmlBody = modEmail.htmlBody + 'Extend deadline to add documentation\n\n';
		modEmail.htmlBody = modEmail.htmlBody + 'Amendment Summary:\n\n';
		modEmail.htmlBody = modEmail.htmlBody + 'Proposal deadline has been changed from 11/18/2016 11:00:00 to 12/16/2016 14:00:00.\n\n\n';
		modEmail.htmlBody = modEmail.htmlBody + 'Amendment number:\n\n';
		modEmail.htmlBody = modEmail.htmlBody + '1\n';

		Account acct = new Account();
		acct.Name = 'Test Account';
		insert acct;

		Contact ct = new Contact();
		ct.LastName = 'Schmo';
		ct.Email ='foo@schmo.com';
		ct.AccountId = acct.Id;
		insert ct;

		SearchTerm__c prim = new SearchTerm__c();
		prim.IsActive__c = true;
		prim.Name = 'graphics';
		prim.TermPadding__c = 'Leading and Trailing spaces';
		insert prim;

		SearchTerm__c sec = new SearchTerm__c();
		sec.IsActive__c = true;
		sec.Name = 'processing';
		sec.TermPadding__c = 'Leading and Trailing spaces';
		insert sec;

		SearchTerm__c ter = new SearchTerm__c();
		ter.IsActive__c = true;
		ter.Name = 'unit';
		ter.TermPadding__c = 'Leading and Trailing spaces';
		insert ter;

		RoutingRule__c rule = new RoutingRule__c();
		rule.PrimaryTerm__c = prim.Id;
		rule.SecondaryTerm__c = sec.Id;
		rule.TertiaryTerm__c = ter.Id;
		rule.IsActive__c = true;
		rule.EmailServiceType__c = 'All';
		insert rule;

		OutboundRecipient__c recip = new OutboundRecipient__c();
		recip.Contact__c = ct.Id;
		recip.RoutingRule__c = rule.Id;
		insert recip;
	}

	@isTest
	static void testEmail()
	{
		setup();
		
		Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
		CIOInboundEmailHandler handler = new CIOInboundEmailHandler();
		handler.handleInboundEmail(email, env);
	}

	@isTest
	static void testMod()
	{
		setup();
		
		Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
		CIOInboundEmailHandler handler = new CIOInboundEmailHandler();
		handler.handleInboundEmail(email, env);

		handler.handleInboundEmail(modEmail, env);
	}

	@isTest
	static void testReprocess()
	{
		setup();

		InboundEmail__c oldEmail = new InboundEmail__c();
		oldEmail.From_Address__c = 'joe@foo.com';
		oldEmail.Subject__c = 'RFQ CS-38204-SB Questions and Answers Released';
		oldEmail.RFQ__c = null;
		oldEmail.Has_Attachment__c = false;
		oldEmail.Outbound_Email_Sent__c = false;
		oldEmail.RFQ_Modification__c = null;
		oldEmail.Text_Body__c = 'Questions and answers for the order RFQ CS-38204-SB has been released\n\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Please visit e-GOS at https://cio.egos.nih.gov for detailed information.\n\n'; 
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'E-GOS ID: CS-38204-SB\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Request: RFQ\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Title: EXXACT Spectrum TXR410-0032R Graphics Processing Unit (GPU)\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Description: 1 EACH - Spectrum TXR410-0032R with the following minimum specifications:256GB DDR44 x GTX 1080 PascalNIVIDIA DIGITSHardware\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'Government Customer: Department of Health and Human Services, NIH, CC\n\n';
		oldEmail.Text_Body__c = oldEmail.Text_Body__c + 'You can view them by logging into e-GOS and clicking on the Documents tab for RFQ CS-38204-SB .\n\n';
		insert oldEmail;
		
		Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
		CIOInboundEmailHandler handler = new CIOInboundEmailHandler();
		handler.isReprocessing = true;
		handler.handleInboundEmail(email, env);
	}

	@isTest
	static void testReprocessError()
	{
		setup();
		
		Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
		CIOInboundEmailHandler handler = new CIOInboundEmailHandler();
		handler.isReprocessing = true;
		Messaging.InboundEmailResult result = handler.handleInboundEmail(email, env);
		System.assert(result.success == false);
	}
}