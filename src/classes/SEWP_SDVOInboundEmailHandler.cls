global with sharing class SEWP_SDVOInboundEmailHandler extends RFQInboundEmailHandler implements Messaging.InboundEmailHandler
{
    global override Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
    {
        return super.handleInboundEmail(email,envelope);
    }

	public override Id getRecordTypeId() 
	{
		Id rtId = null;
        try
        {
            RecordType rt = [select Id, Name, DeveloperName, sObjectType from RecordType where sObjectType='RFQ_RFI__c' and IsActive = TRUE and DeveloperName='SEWP_V_SDVO_SB'];
            rtId = rt.Id;
        }
        catch(Exception e)
        {
            rtId = null;
            System.debug('**** SEWP_SDVOInboundEmailHandler Could not retrieve SEWP_V_SDVO_SB RecordType.');
        }
        return rtId;
	}

	public override String getContractType()
    {
        return 'SEWP V (SDVO/SB)';
    }

    public override String getInboundEmailServiceType()
    {
        return 'SEWP V (SDVO/SB)';
    }
}