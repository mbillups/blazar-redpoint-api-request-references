@isTest
private class TestTextParser
{
	@isTest
	static void testParse()
	{
		String inText = 'This is a test of the Emergency Broadcasting System. This is only a test';
		String term = 'emergency';

		Boolean bool = TextParser.findWord(inText, term);
		System.assert(bool);
	}

	@isTest
	static void testParseFalse()
	{
		String inText = 'This is a test of the Emergency Broadcasting System. This is only a test';
		String term = 'emrgency';

		Boolean bool = TextParser.findWord(inText, term);
		System.assert(bool==false);
	}

	@isTest
	static void testParseWithPadding()
	{
		String inText = 'Tattoo too';
		String term = ' tat '; // this shouldn't be found

		Boolean bool = TextParser.findWord(inText, term);
		System.assert(bool==false);

		term = 'tat'; // this should be found

		bool = TextParser.findWord(inText, term);
		System.assert(bool);
	}

	@isTest
	static void testMulti()
	{
		String inText = 'This is a test of the Emergency Broadcasting System. This is only a test';
		String term1 = 'emergency';
		String term2 = 'Broadcasting';

		List<Boolean> results = TextParser.findWordsInOrder(inText, new List<String>{term1, term2});
		System.assert(results.get(0) && results.get(1));
	}

	@isTest
	static void testMultiFindFirst()
	{
		String inText = 'This is a test of the Emergency Broadcasting System. This is only a test';
		String term1 = 'emergency';
		String term2 = 'Brad';

		List<Boolean> results = TextParser.findWordsInOrder(inText, new List<String>{term1, term2});
		System.assert(results.get(0)==true && results.get(1)==false);
	}

	@isTest
	static void testInBetween()
	{
		String inText = 'EMAIL : foo@foo.com\n';
		
		String val = TextParser.getValueBetween(inText, 'EMAIL :', '\n'); 
		System.assert(val == 'foo@foo.com');
	}

	@isTest
	static void testGetDate()
	{
		String inText = '27-DEC-2016';
		
		Date val = TextParser.getDate(inText); 
		System.assert(val != null);
	}

	@isTest
	static void testGetDateTime()
	{
		String inText = '27-DEC-2016 09:16';
		
		DateTime val = TextParser.getDateTime(inText); 
		System.assert(val != null);
	}

	@isTest
	static void testPadding()
	{
		String inText = 'Tattoo';
		String result = TextParser.addPaddingToTerm('Leading space', inText);
		System.assert(result == ' Tattoo');

		inText = 'Tattoo';
		result = TextParser.addPaddingToTerm('Trailing space', inText);
		System.assert(result == 'Tattoo ');

		inText = 'Tattoo';
		result = TextParser.addPaddingToTerm('Leading and Trailing spaces', inText);
		System.assert(result == ' Tattoo ');
	}

	@isTest
	static void testAsposePadding()
	{
		String inText = 'Tattoo';
		String result = TextParser.addAsposePaddingToTerm(inText);
		System.assert(result == '"Tattoo"');

		result = TextParser.removeAsposePaddingFromTerm(result);
		System.assert(result == 'Tattoo');
	}
}