trigger RoutingRuleTrigger on RoutingRule__c (before insert, before update) 
{
	// Make sure new rules are unique. Do this by checking to make sure any ACTIVE rules don't have the same terms

	List<RoutingRule__c> existingRules = [select Id, Name, IsActive__c, AllTerms__c, EmailServiceType__c from RoutingRule__c where IsActive__c=true];

	if(existingRules!=null && existingRules.size() > 0)
	{
		for(RoutingRule__c rule : Trigger.new)
		{
			if(rule.IsActive__c)
			{
				for(RoutingRule__c existingRule : existingRules)
				{
					if(rule.Id != existingRule.Id)
					{
						if(rule.AllTerms__c.trim() == existingRule.AllTerms__c.trim() && rule.EmailServiceType__c == existingRule.EmailServiceType__c)
						{
							rule.addError('There is already a Routing Rule with these terms and service types. Duplicates are not allowed.');
						}
					}
				}
			}
		}
	}
}